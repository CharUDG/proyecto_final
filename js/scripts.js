
	 var hazardFire = false;
	 var hazardFlood = false;
	 var hazardTree = false;
	 var hazardCar = false;
	 var hazardNoPower = false;
	 var hazardNoWater = false;
	 var hazardZoo = false;
	
	

	$(document).ready(function() {
		$(window).on("load resize", function() {
			var alturaBuscador = $(".buscador").outerHeight(true),
				alturaVentana = $(window).height(),
				alturaMapa = alturaVentana - alturaBuscador;
			
			$("#mapa-geocoder").css("height", alturaMapa+"px");

			var current = navigator.geolocation.getCurrentPosition(function(position) {
		  haz_algo(position.coords.latitude, position.coords.longitude);
		});

		function haz_algo(lat, lon){
			console.log("Lat: " + lat + "\nLon: " +lon);

			var geocoder = new google.maps.Geocoder();

			var map = new google.maps.Map(document.getElementById("mapa-geocoder"), {
			  zoom: 16,
			  scrollwheel: true,
			  mapTypeId: google.maps.MapTypeId.ROADMAP
			});
		
			var direccion = lat.toString() + " " + lon.toString();
			console.log(direccion.toString());

			geocoder.geocode({'address': direccion.toString()}, function(results, status) {
				if (status === 'OK') {
					var dir2 = results[0].formatted_address;
					console.log("Direccion: " + dir2);
					document.getElementById("direccion").value = dir2;
					var resultados = results[0].geometry.location,
						resultados_lat = resultados.lat(),
						resultados_long = resultados.lng();
					
					map.setCenter(results[0].geometry.location);
					var marker = new google.maps.Marker({
						map: map,
						position: results[0].geometry.location
					});
				} else {
					var mensajeError = "";
					if (status === "ZERO_RESULTS") {
						mensajeError = "No hubo resultados para la dirección ingresada.";
					} else if (status === "OVER_QUERY_LIMIT" || status === "REQUEST_DENIED" || status === "UNKNOWN_ERROR") {
						mensajeError = "Error general del mapa.";
					} else if (status === "INVALID_REQUEST") {
						mensajeError = "Error de la web. Contacte con Name Agency.";
					}
					alert(mensajeError);
				}
			});
		}
		});

		
		

		function localizar(elemento,direccion) {

		var geocoder = new google.maps.Geocoder();

		var map = new google.maps.Map(document.getElementById(elemento), {
		  zoom: 16,
		  scrollwheel: true,
		  mapTypeId: google.maps.MapTypeId.ROADMAP
		});
		

		geocoder.geocode({'address': direccion}, function(results, status) {
			if (status === 'OK') {
				var resultados = results[0].geometry.location,
					resultados_lat = resultados.lat(),
					resultados_long = resultados.lng();
				
				map.setCenter(results[0].geometry.location);
				var marker = new google.maps.Marker({
					map: map,
					position: results[0].geometry.location
				});
			} else {
				var mensajeError = "";
				if (status === "ZERO_RESULTS") {
					mensajeError = "No hubo resultados para la dirección ingresada.";
				} else if (status === "OVER_QUERY_LIMIT" || status === "REQUEST_DENIED" || status === "UNKNOWN_ERROR") {
					mensajeError = "Error general del mapa.";
				} else if (status === "INVALID_REQUEST") {
					mensajeError = "Error de la web. Contacte con Name Agency.";
				}
				alert(mensajeError);
			}
		});
	}
	
    $.getScript("https://maps.googleapis.com/maps/api/js?key=AIzaSyCxlCMLLZHMvSRGK6t03i2OyQ2zkkhbhPc&libraries=places&v=weekly&callback=auto", function() {
		$("#buscar").click(function() {
            var direccion = $("#direccion").val();
			if (direccion !== "") {
				localizar("mapa-geocoder", direccion);
			}
        });
	});

	});

	
		function auto(){
			var input= document.getElementById("direccion");

	    	var autocomplete = new google.maps.places.Autocomplete(input,{
	            types: ["geocode"]
	          });
		}
	

	
		$(document).ready(function(){
			 $("#fire").click(function(){
			 	hazardFire = true;
			 	console.log(hazardFire);
			   $("#fire").fadeOut(1000);
			   //$("#place").fadeIn(1000);
			   $("#place").css("display", "inline-block").fadeIn(1000);
			});
		});

		$(document).ready(function(){
			 $("#place").click(function(){
			 	hazardFire = false;
			   $("#fire").fadeIn(1000);
			   //$("#place").fadeIn(1000);
			   $("#place").css("display", "none")
			});
		});
	

	
		$(document).ready(function(){
			 $("#flood").click(function(){
			 	hazardFlood = true;
			   $("#flood").fadeOut(1000);
			   $("#place2").css("display", "inline-block")
			});
		});

		$(document).ready(function(){
			 $("#place2").click(function(){
			 	hazardFlood = false;
			   $("#flood").fadeIn(1000);
			   $("#place2").css("display", "none")
			});
		});

	

	
		$(document).ready(function(){
			 $("#tree").click(function(){
			 	hazardTree = true;
			   $("#tree").fadeOut(1000);
			   $("#place3").css("display", "inline-block")
			});
		});

		$(document).ready(function(){
			 $("#place3").click(function(){
			 	hazardTree = false;
			   $("#tree").fadeIn(1000);
			   $("#place3").css("display", "none")
			});
		});
	

	
		$(document).ready(function(){
			 $("#car").click(function(){
			 	hazardCar = true;
			   $("#car").fadeOut(1000);
			   $("#place4").css("display", "inline-block")
			});
		});

		$(document).ready(function(){
			 $("#place4").click(function(){
			 	hazardCar = false;
			   $("#car").fadeIn(1000);
			   $("#place4").css("display", "none")
			});
		});
	

	
		$(document).ready(function(){
			 $("#no-power").click(function(){
			 	hazardNoPower = true;
			   $("#no-power").fadeOut(1000);
			   $("#place5").css("display", "inline-block")
			});
		});

		$(document).ready(function(){
			 $("#place5").click(function(){
			 	hazardNoPower = false;
			   $("#no-power").fadeIn(1000);
			   $("#place5").css("display", "none")
			});
		});
	

	
		$(document).ready(function(){
			 $("#no-water").click(function(){
			 	hazardNoWater = true;
			   $("#no-water").fadeOut(1000);
			   $("#place6").css("display", "inline-block")
			});
		});

		$(document).ready(function(){
			 $("#place6").click(function(){
			 	hazardNoWater = false;
			   $("#no-water").fadeIn(1000);
			   $("#place6").css("display", "none")
			});
		});
	

	
		$(document).ready(function(){
			 $("#zoo-hazard").click(function(){
			 	hazardZoo = true;
			   $("#zoo-hazard").fadeOut(1000);
			   $("#place7").css("display", "inline-block")
			});
		});

		$(document).ready(function(){
			 $("#place7").click(function(){
			 	hazardZoo = false;
			   $("#zoo-hazard").fadeIn(1000);
			   $("#place7").css("display", "none")
			});
		});
	
	//Script de login
	
	
		firebase.auth().onAuthStateChanged(function(user) {


		  if (user) {
		    // User is signed in.
		    var usuario = localStorage.getItem('corr');
			console.log(usuario);
			document.getElementById("usuario").innerHTML = document.getElementById("usuario").innerHTML + usuario;
		    
		  } else {
		    // No user is signed in.
		    document.getElementById("menu").style.display = "none";
			window.document.location = './login.html';
		  }

		});


		function logout(){
			firebase.auth().signOut();
		}

		//codigo encargado de almacenar los reportes
		var firestore = firebase.firestore().collection("reports");

		//convertir los campos de texto a query
		const inputAddress = document.querySelector("#direccion");
 		const descrip = document.querySelector("#descripcion");
		const sendButton = document.querySelector("#fin");
		const loadButton = document.querySelector("#load");

		const docRef = firestore;

		//se ejecuta  cuando presionamos el voton de enviar (crear un reporte)
	  	sendButton.addEventListener("click", function(){
	  	//obtenemos el correo del usuario
	  	var id = localStorage.getItem('corr');
	  	//const textIdToSave = dir;
	  	const addressToSave = inputAddress.value;
	  	const fireStatus = hazardFire;
	  	const floodStatus = hazardFlood;
	  	const treeStatus = hazardTree;
	  	const carStatus = hazardCar;
	  	const noPowerStatus = hazardNoPower;
	  	const noWaterStatus = hazardNoWater;
	  	const zooStatus = hazardZoo;
	  	const desToSave = descrip.value;
	  	const docRef = firestore;

	  	if (addressToSave != "" && desToSave != "") {


	  	console.log("Im gooing to save " + id + " to Firestore");
	  	console.log("Im gooing to save " + addressToSave + " to Firestore");
	  	console.log("Im gooing to save " + fireStatus + " to Firestore");
	  	console.log("Im gooing to save " + floodStatus + " to Firestore");
	  	console.log("Im gooing to save " + treeStatus + " to Firestore");
	  	console.log("Im gooing to save " + carStatus + " to Firestore");
	  	console.log("Im gooing to save " + noPowerStatus + " to Firestore");
	  	console.log("Im gooing to save " + noWaterStatus + " to Firestore");
	  	console.log("Im gooing to save " + zooStatus + " to Firestore");
	  	console.log("Im gooing to save " + desToSave + " to Firestore");

	  	docRef.add({
	  		ID: id,
	  		Address: addressToSave,
	  		Fire: fireStatus,
	  		Flood: floodStatus,
	  		Tree: treeStatus,
	  		Car: carStatus,
	  		NoPower: noPowerStatus,
	  		NoWater: noWaterStatus,
	  		ZooHazard: zooStatus,
	  		Description: desToSave,
	  		Datetime: new Date()
	  	}).then(function(){
	  		console.log("Status saved!");
	  	}).catch(function(){
	  		console.log("Got an error: ", error);
	  	});

	  	alert("Reporte guardado! gracias por informar.");
	  	inputAddress.value = "";
	  	descrip.value = "";
	  	document.getElementById("sticky").click();	

	  }else{
	  	alert("Asegurate de tener una direccion y una descripcion.");
	  }
	  	
	  })

	  	loadButton.addEventListener("click", function(){
	  		//window.document.location = './reports.html';
	  		var myWindow = window.open("reports.html", "newWindow", "width=500,height=700");
	  	
	  });