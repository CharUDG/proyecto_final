

firebase.auth().onAuthStateChanged(function(user) {


		  if (user) {
		    // User is signed in.
		    window.document.location = './main.html';
		    document.getElementById("main-div").style.display = "none";
		    var user = firebase.auth().currentUser;
		    if (user != null){
		    	//aqui obtenemos el ID del usuario
		    	var email_id = user.email;
		    	localStorage.removeItem('corr');
	  			localStorage.setItem('corr',email_id);
		    }
		  } else {
		    // No user is signed in.
		    console.log("Verificando..");
		    document.getElementById("main-div").style.display = "block";
		  }

		});

		function login(){
			console.log("Logeando...");
			var userEmail = document.getElementById("email").value;
			var userPass = document.getElementById("pass").value;
			console.log("E-mail: " + userEmail);
			console.log("Password: " + userPass);
			firebase.auth().signInWithEmailAndPassword(userEmail, userPass).catch(function(error) {
			  // Handle Errors here.
			  var errorCode = error.code;
			  var errorMessage = error.message;

			  window.alert("Error: " + errorMessage);
			  // ...
			});
		}

		function create(){
			var userEmail2 = document.getElementById("email-signUp").value;
			var userPass2 = document.getElementById("pass-signUp").value;
			var userPass3 = document.getElementById("pass-signUp-con").value;

			if(userPass2 == userPass3 && userPass2.length >=6){
				firebase.auth().createUserWithEmailAndPassword(userEmail2, userPass2).catch(function(error) {
				  // Handle Errors here.
				  var errorCode = error.code;
				  var errorMessage = error.message;
				  // ...
				});
			}else{
				alert("Asegurate de que las contraseñas coincidan y que tengan minimo 6 caracteres.");
			}
		}

		function signUp(){
			window.document.location = './signup.html';
		}

		/**/