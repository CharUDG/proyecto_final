
var cont = 1;
var firestore = firebase.firestore().collection("reports");
	  const docRef = firestore;
	  //obtenemos el ID guardado en el localStorage
	  var corr2 = localStorage.getItem('corr').toString();
	  console.log(corr2 + "hola");
	  //buscamos el ID y mostramos los reportes relacionados
	  docRef.where("ID", "==", corr2)
    	.get()
	    .then(function(querySnapshot) {
	        querySnapshot.forEach(function(doc) {
	            // doc.data() is never undefined for query doc snapshots
	            console.log(doc.id, " => ", doc.data());
	            const myData = doc.data();
		        console.log("Datos: " + myData.Email);
		        print(myData);
		        
	        });
	        
	    })
	    .catch(function(error) {
	        console.log("Error getting documents: ", error);
	    });

	    function print(myData){
	    //Guardamos los datos en variables y procedemos a imprimirlos creando nuevas etiquetas html
		var ID = myData.ID;
	  	var dateTime = myData.Datetime.toDate();
	  	var address = myData.Address;
	  	var fire = myData.Fire;
	  	var flood = myData.Flood;
	  	var tree = myData.Tree;
	  	var car = myData.Car;
	  	var noPower = myData.NoPower;
	  	var noWater = myData.NoWater;
	  	var zoo = myData.ZooHazard;
	  	var des = myData.Description;

	  	var container_div = document.createElement("div");
	  	var card_div = document.createElement("div");
	  	var specs_div = document.createElement("div");

	  	container_div.setAttribute("class","container_cards");
	  	card_div.setAttribute("class","card");
	  	specs_div.setAttribute("class","specifications");

	  	var tag_h = document.createElement("h1");
		var tag = document.createElement("p");
		var tag2 = document.createElement("p");
		var tag3 = document.createElement("p");
		var tag11 = document.createElement("p");
		var hr1 = document.createElement("hr");
		var hr2 = document.createElement("hr");
		var hr3 = document.createElement("hr");
		var hr4 = document.createElement("hr");

		

		var text_h = document.createTextNode("Reporte #" + cont);
	   	var text = document.createTextNode("ID: " + ID);
	   	var text2 = document.createTextNode("FECHA: " + dateTime);
	   	var text3 = document.createTextNode("DIRECCION: " + address);
	   	var text11 = document.createTextNode("DESCRIPCION: " + des);


	   	tag_h.appendChild(text_h);
	   	tag.appendChild(text);
	   	tag2.appendChild(text2);
	   	tag3.appendChild(text3);
		tag11.appendChild(text11);

	   	var element = document.getElementById("main");
	   	
	   	
	   	specs_div.appendChild(tag);
	   	specs_div.appendChild(hr1);
	   	specs_div.appendChild(tag2);
	   	specs_div.appendChild(hr2);
	   	specs_div.appendChild(tag3);
	   	specs_div.appendChild(hr3);
	   	specs_div.appendChild(tag11);
	   	specs_div.appendChild(hr4);
	   	card_div.appendChild(tag_h);
	   	card_div.appendChild(specs_div);
	   	container_div.appendChild(card_div);
	   	element.appendChild(container_div);
	   	
	
	   	if (fire == true){
			var tagFire = document.createElement("img");
			tagFire.setAttribute("src", "HAZARDS/fire.png");
			tagFire.setAttribute("alt", "sp1");
			tagFire.setAttribute("width", "64");
			tagFire.setAttribute("height", "64");
			specs_div.appendChild(tagFire);
		}
		if (flood == true){
			var tagFlood = document.createElement("img");
			tagFlood.setAttribute("src", "HAZARDS/flood.png");
			tagFlood.setAttribute("alt", "sp1");
			tagFlood.setAttribute("width", "64");
			tagFlood.setAttribute("height", "64");
			specs_div.appendChild(tagFlood);
		}
		if (tree == true){
			var tagTree = document.createElement("img");
			tagTree.setAttribute("src", "HAZARDS/tree.png");
			tagTree.setAttribute("alt", "sp1");
			tagTree.setAttribute("width", "64");
			tagTree.setAttribute("height", "64");
			specs_div.appendChild(tagTree);
		}
		if (car == true){
			var tagCar = document.createElement("img");
			tagCar.setAttribute("src", "HAZARDS/car.png");
			tagCar.setAttribute("alt", "sp1");
			tagCar.setAttribute("width", "64");
			tagCar.setAttribute("height", "64");
			specs_div.appendChild(tagCar);
		}
		if (noPower == true){
			var tagPower = document.createElement("img");
			tagPower.setAttribute("src", "HAZARDS/nopower.png");
			tagPower.setAttribute("alt", "sp1");
			tagPower.setAttribute("width", "64");
			tagPower.setAttribute("height", "64");
			specs_div.appendChild(tagPower);
		}
		if (noWater == true){
			var tagWater = document.createElement("img");
			tagWater.setAttribute("src", "HAZARDS/nowater.png");
			tagWater.setAttribute("alt", "sp1");
			tagWater.setAttribute("width", "64");
			tagWater.setAttribute("height", "64");
			specs_div.appendChild(tagWater);
		}
		if (zoo == true){
			var tagZoo = document.createElement("img");
			tagZoo.setAttribute("src", "HAZARDS/zoo.png");
			tagZoo.setAttribute("alt", "sp1");
			tagZoo.setAttribute("width", "64");
			tagZoo.setAttribute("height", "64");
			specs_div.appendChild(tagZoo);
		}

	   	cont++;
	}